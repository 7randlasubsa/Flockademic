interface OaLocation {
  host_type: 'publisher' | 'repository';
  is_best: boolean;
  license: null
    | 'cc-by'
    | 'cc-by-sa'
    | 'cc-by-nd'
    | 'cc-by-nc'
    | 'cc-by-nc-sa'
    | 'cc-by-nc-nd'
    | 'implied-oa'
    | string;
  url: string;
  url_for_pdf: null | string;
}

interface GetOaLocationsValidResponse {
  best_oa_location: null | OaLocation;
  doi: string;
  doi_url: string;
  is_oa: boolean;
  journal_is_in_doaj: boolean;
  journal_is_oa: boolean;
  journal_name: string;
  oa_locations: OaLocation[];
  published_date: string;
  publisher: string;
  title: string;
  z_authors: Array<{
    ORCID?: string;
    affliation?: Array<{ name: string }>;
    'authenticated-orcid'?: boolean;
    family: string;
    given: string;
  }>;
}

interface GetOaLocationsInvalidResponse {
  HTTP_status_code: number;
  error: boolean;
  message: string;
}

export type GetOaLocationsResponse = GetOaLocationsValidResponse | GetOaLocationsInvalidResponse;

export function isValidOaLocationsResponse(response: GetOaLocationsResponse): response is GetOaLocationsValidResponse {
  return typeof (response as GetOaLocationsValidResponse).doi !== 'undefined';
}
