import { Session } from '../../interfaces/Session';
import { Middleware, Request } from '../faas';
import { verifyJwt } from '../verifyJwt';

export interface SessionContext { session: Session | Error; }

export function withSession<RequestBody>(next: Middleware<RequestBody>): Middleware<RequestBody> {
  const newMiddleware: Middleware<RequestBody> = (request: Request<RequestBody & { jwt?: string }>) => {
    let jwt;

    const authorizationHeaders =
      Object.keys(request.headers || {})
      .filter((header) => header.toLowerCase() === 'authorization');
    if (authorizationHeaders.length > 0) {
      const values = request.headers[authorizationHeaders[0]].split(/\s+/);
      if (values[0].toLowerCase() === 'bearer') {
        jwt = values[1];
      }
    }

    const session = verifyJwt(jwt);

    const context: Request<RequestBody> & SessionContext = {
      ...request,
      session,
    };

    return next(context);
  };

  return newMiddleware;
}
