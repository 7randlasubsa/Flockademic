import { BareOrcidButton, OrcidButtonProps } from '../../src/components/orcidButton/component';

import { mount } from 'enzyme';
import toJson from 'enzyme-to-json';
import * as React from 'react';
import { MemoryRouter, withRouter } from 'react-router';

const ButtonWithRouter = withRouter<OrcidButtonProps>(BareOrcidButton);

const mockProps: OrcidButtonProps & { children: React.ReactNode } = {
  children: [],
  state: { verifying: false },
};
function initialiseButton({
  state = mockProps.state,
  children = mockProps.children,
  className = '',
  location = '/arbitrary',
}: Partial<OrcidButtonProps & { children: React.ReactNode } & { location: string | Partial<Location> }> = {}) {
  return (
    <MemoryRouter initialEntries={[ location ]}>
      <ButtonWithRouter
        className={className}
        state={state}
      >
        {children}
      </ButtonWithRouter>
    </MemoryRouter>
  );
}

it('should display a button to sign in to ORCID with the given label', () => {
  const button = mount(initialiseButton({ children: 'Arbitrary label' })).find(BareOrcidButton);

  expect(button.find('a')).toExist();
  expect(button.text()).toBe('Arbitrary label');
});

it('should transfer class names to the actual link element', () => {
  const button = mount(initialiseButton({ className: 'some-class' })).find(BareOrcidButton);

  expect(button.find('a')).toExist();
  expect(button.find('a').prop('className')).toBe('some-class');
});

it('should display a spinner while verifying', (done) => {
  const wrapper = mount(initialiseButton({ state: { verifying: true } }));

  setImmediate(() => {
    expect(wrapper.find('Spinner')).toExist();
    done();
  });
});

it('should remove the spinner when verification failed', (done) => {
  const wrapper = mount(initialiseButton({
    children: 'Some content',
    state: { verifying: false, error: new Error('Arbitrary error') },
  }));

  setImmediate(() => {
    expect(wrapper.find('Spinner')).not.toExist();
    expect(wrapper.find(BareOrcidButton).text()).toBe('Some content');
    done();
  });
});

it('should display a verified ORCID', (done) => {
  const wrapper = mount(initialiseButton({
    children: 'Some content',
    state: { verifying: false, orcid: 'some-orcid' },
  }));

  setImmediate(() => {
    expect(wrapper.find('Spinner')).not.toExist();
    expect(wrapper.find('Link[to="/profile/some-orcid"]')).toExist();
    expect(wrapper.text()).toBe('Your profile');
    done();
  });
});
