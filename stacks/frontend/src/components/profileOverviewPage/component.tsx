import './styles.scss';

import * as React from 'react';
import * as ReactGA from 'react-ga';
import { connect } from 'react-redux';
import { RouteComponentProps } from 'react-router-dom';
import { createSelector, Selector } from 'reselect';

import { Redirect } from 'react-router';
import { Person } from '../../../../../lib/interfaces/Person';
import { ScholarlyArticle } from '../../../../../lib/interfaces/ScholarlyArticle';
import { isOrcidWorkLink } from '../../../../../lib/utils/orcid';
import { AppState } from '../../ducks/app';
import { getAccountId, getProfiles } from '../../services/account';
import { getProfile as getOrcidProfile } from '../../services/orcid';
import { getScholarlyArticlesForAccount } from '../../services/periodical';
import { ErrorPage } from '../errorPage/component';
import { ProfileOverview } from '../profileOverview/component';
import { Spinner } from '../spinner/component';

interface ProfileOverviewPageState {
  articles?: null | Array<Partial<ScholarlyArticle>>;
  profile?: null | Partial<Person>;
  orcidProfile?: null | Partial<Person>;
  orcidArticles?: null | Array<Partial<ScholarlyArticle>>;
}
interface StateProps {
  orcid?: string;
  verifyingOrcid: boolean;
}
interface DispatchProps {}
interface RouteParams { orcid?: string; }
type RouteProps = RouteComponentProps<RouteParams> | undefined;

type ReduxProps = StateProps & DispatchProps;
export type Props = ReduxProps & RouteProps;

export class BareProfileOverviewPage
  extends React.Component<
  Props,
  ProfileOverviewPageState
> {
  public state: ProfileOverviewPageState = {};
  private articlesSelector: Selector<ProfileOverviewPageState, Array<Partial<ScholarlyArticle>> | null | undefined>;
  private profileSelector: Selector<ProfileOverviewPageState, Partial<Person> | null>;

  constructor(props: any) {
    super(props);

    this.articlesSelector = createSelector<
      ProfileOverviewPageState,
      Array<Partial<ScholarlyArticle>> | undefined | null,
      Array<Partial<ScholarlyArticle>> | undefined | null,
      Array<Partial<ScholarlyArticle>> | undefined | null
    >(
      (state) => state.articles,
      (state) => state.orcidArticles,
      (articles, orcidArticles) => {
        // If internal articles are null or undefined, we don't want to display any articles.
        // If external (ORCID) articles are null or undefined, only show internal articles.
        if (!articles || !orcidArticles) {
          return articles;
        }

        const linkedArticles = articles
          .filter((article) => article.sameAs && isOrcidWorkLink(article.sameAs))
          // Type cast here because TypeScript doesn't know that filter has removed all undefined `sameAs`s
          .map((article) => article.sameAs as string);

        const unlinkedOrcidArticles = orcidArticles
          .filter((article) => article.sameAs && linkedArticles.indexOf(article.sameAs) === -1);

        return articles.concat(unlinkedOrcidArticles);
      },
    );

    this.profileSelector = createSelector<
      ProfileOverviewPageState,
      Partial<Person> | undefined | null,
      Partial<Person> | undefined | null,
      Partial<Person> | null
    >(
      (state) => state.profile,
      (state) => state.orcidProfile,
      (profile, orcidProfile) => {
        if (profile === null && orcidProfile === null) {
          return null;
        }

        return {
          ...orcidProfile,
          ...profile,
          // The internal Flockademic profile only includes a link to the user's ORCID,
          // which is not interesting enough to display here - and certainly shouldn't override user-defined websites:
          sameAs: (orcidProfile && orcidProfile.sameAs) ? orcidProfile.sameAs : undefined,
        };
      },
    );
  }

  public componentDidMount() {
    // Fetch account details (getAccountId) and ORCID profile (getOrcidProfile) in parallel
    const orcid = this.props.match.params.orcid || this.props.orcid;
    if (orcid) {
      this.fetchAccountData(orcid);
    }
  }

  public componentWillReceiveProps(newProps: Props) {
    // As soon as we know we're going to redirect to the current user's profile, start fetching their data
    // (We can't do this upon mounting, since the ORCID is hydrated from localstorage asynchronously.)
    if (
      !this.props.match.params.orcid
      && !this.props.orcid
      && !newProps.verifyingOrcid
      && newProps.orcid
    ) {
      this.fetchAccountData(newProps.orcid);
    }
  }

  public render() {
    const articles = this.articlesSelector(this.state);
    const profile = this.profileSelector(this.state);

    // If the profile could not be loaded,
    // or if we're trying to load the current user's own profile but nobody is logged in:
    if (profile === null || (!this.props.match.params.orcid && !this.props.orcid && !this.props.verifyingOrcid)) {
      return (
        <ErrorPage
          title="Could not load profile details"
          url={this.props.match.url}
        >
          This profile could not be found, please try again.
        </ErrorPage>
      );
    }

    // Redirect to the current user's profile page if no profile was specified
    // (Note that we've already started fetching that user's details above,
    // as the same route still matches so this component is still displayed.)
    if (!this.props.match.params.orcid && this.props.orcid) {
      return (
        <Redirect to={`/profile/${this.props.orcid}`}/>
      );
    }

    if (typeof profile.name === 'undefined') {
      return (
        <div className="container">
          <Spinner/>
        </div>
      );
    }

    const isFlockademicUser = (typeof this.state.profile !== 'undefined') && (this.state.profile !== null);

    return (
      <ProfileOverview
        articles={articles}
        person={profile}
        url={this.props.match.url}
        isFlockademicUser={isFlockademicUser}
        currentUser={this.props.orcid}
        profileIsCurrentUser={this.props.orcid === this.props.match.params.orcid}
      />
    );
  }

  private fetchAccountData(orcid: string) {
    getAccountId(orcid)
    .then((accountId) => {
      this.setState((prevState) => ({
        profile: {
          ...prevState.profile,
          identifier: accountId,
        },
      }));

      // Get profile details (getProfiles) and published articles (getScholarlyArticlesForAccount)
      // for this account in parallel
      getProfiles([ accountId ])
      .then((profiles) => {
        if (profiles.length !== 1) {
          this.setState({ profile: null });

          return;
        }

        this.setState({ profile: profiles[0] });
      })
      .catch(() => {
        this.setState({ profile: null });
      });

      getScholarlyArticlesForAccount(accountId)
      .then((articles) => {
        this.setState({ articles });
      })
      .catch(() => {
        this.setState({ articles: null });
      });
    })
    .catch(() => {
      this.setState({ articles: [], profile: null });
    });

    getOrcidProfile(orcid)
    .then(([ orcidProfile, orcidArticles ]) => {
      ReactGA.event({
        action: 'Load ORCID profile',
        category: 'View Profile',
        label: 'ORCID works',
        value: orcidArticles.length,
      });
      this.setState({ orcidArticles, orcidProfile });
    })
    .catch(() => {
      this.setState({ orcidArticles: null, orcidProfile: null });
    });
  }
}

// The next function is trivial (and forced to be correct through the types),
// but testing it requires inspecting the props of the connected component and mocking the store.
// Figuring that out is not worth it, so we just don't test this:
/* istanbul ignore next */
function mapStateToProps(state: AppState): StateProps {
  return {
    orcid: state.account.orcid,
    verifyingOrcid: state.account.verifying,
  };
}
export const ProfileOverviewPage = connect<
  StateProps,
  DispatchProps,
  RouteProps,
  AppState
>(
    mapStateToProps,
  )(
    BareProfileOverviewPage,
  );
