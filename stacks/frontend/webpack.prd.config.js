var makeBaseConfig = require('./webpack.common.config');

var webpack = require('webpack');
var path = require('path');
var CleanWebpackPlugin = require('clean-webpack-plugin');
var PrerenderPlugin = require('./build/prerender-plugin');
var RobotstxtPlugin = require("robotstxt-webpack-plugin").default;
var HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = function makeWebpackConfig (env) {
  var config = makeBaseConfig(env);

  config.devtool = 'source-map';

  config.entry.app = './src/app';

  config.output = {
    filename: '[name].[chunkhash].js',
    path: path.join(__dirname, 'dist'),
    publicPath: '/',
    // We need to use UMD because we're outputting both a bundle for the browser,
    // and a module that will be used for prerendering the app after the build.
    // UMD enables the modules to be loaded in both (since we can't configure
    // different libraryTargets per chunk).
    libraryTarget: 'umd',
  };

  config.plugins.push(
    new CleanWebpackPlugin(['dist']),

    // Reference: http://webpack.github.io/docs/list-of-plugins.html#uglifyjsplugin
    // Minify all javascript, switch loaders to minimizing mode
    new webpack.optimize.UglifyJsPlugin({
      compress: { warnings: true },
      sourceMap: true,
    }),

    // Reference: https://facebook.github.io/react/docs/optimizing-performance.html#use-the-production-build
    // Tell React that this build is to be used in production
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: JSON.stringify('production'),
      },
    }),

    // Reference: https://www.npmjs.com/package/robotstxt-webpack-plugin
    new RobotstxtPlugin({
      policy: [
        {
          userAgent: '*',
          disallow: [ '/journals/new', '/journal/*/submit', '/dashboard' ],
        },
      ],
    }),

    // Reference: https://webpack.js.org/plugins/loader-options-plugin/
    // Tell all loaders that they need to enable minification
    new webpack.LoaderOptionsPlugin({
      minimize: true,
      debug: false,
    }),

    // This HTML file will be served as the 404 page, to render the SPA there (with client-side routing).
    // The main difference is that it doesn't render the entire index page without Javascript,
    // which would be replaced by the contents of the actual page. Instead, it only renders the shell.
    new HtmlWebpackPlugin({
      template: 'src/index.html',
      filename: 'fallback.html',
      inject: 'body',
      minify: {
        collapseWhitespace: true,
      },
      // `app` is only compiled so it can be prerendered and inserted into the HTML later:
      excludeChunks: ['app'],
    }),
    // Do an intial render of the app and insert that into index.html
    new PrerenderPlugin({
      placeholder: 'Loading&hellip; (Requires Javascript)',
      elementOutputFile: /app(.*).js/,
      htmlOutputFile: /index.html/,
      fallbackOutputFile: /fallback.html/,
      renderIndex: true,
    })
  );

  return config;

};
