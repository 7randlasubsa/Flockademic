import { InitialisedPeriodical } from '../../../../lib/interfaces/Periodical';
import { Database } from '../../../../lib/lambda/middleware/withDatabase';

// This file is ignored for test coverage in the Jest configuration
// since it is merely a translation of Javascript objects to SQL queries.
export async function fetchPeriodicals(
  database: Database,
): Promise<InitialisedPeriodical[]> {
  interface Row {
    date_published: Date;
    identifier: string;
    name: string;
    description?: string;
    headline?: string;
  }

  const result = await database.manyOrNone<Row>(
    'SELECT'
    + ' p.identifier, p.name, p.description, p.headline, p.date_published'
    + ' FROM periodicals p'
    + ' LEFT JOIN scholarly_articles_part_of articles'
    + ' ON articles.is_part_of=p.uuid'
    + ' AND articles.scholarly_article=('
      + 'SELECT'
      + ' articles.scholarly_article'
      + ' FROM scholarly_articles_part_of articles'
      + ' WHERE articles.is_part_of=p.uuid'
      + ' ORDER BY articles.date_published DESC'
      + ' LIMIT 1'
    + ')'
    + ' WHERE p.date_published IS NOT NULL AND identifier IS NOT NULL AND name IS NOT NULL'
    + ' ORDER BY articles.date_published DESC NULLS LAST',
  );

  const periodicals = result.map((row) => {
    const periodical: InitialisedPeriodical = {
      datePublished: row.date_published.toISOString(),
      description: row.description,
      headline: row.headline,
      identifier: row.identifier,
      name: row.name,
    };

    return periodical;
  });

  return periodicals;
}
