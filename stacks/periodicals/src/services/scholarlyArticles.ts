import { PublishedScholarlyArticle, ScholarlyArticle } from '../../../../lib/interfaces/ScholarlyArticle';
import { Database } from '../../../../lib/lambda/middleware/withDatabase';

export async function fetchNewestIndependentScholarlyArticles(
  database: Database,
  limit: number = 5,
): Promise<PublishedScholarlyArticle[]> {
  interface Row {
    identifier: string;
    name: string;
    description: string;
    author: string;
    date_published: Date;
  }
  const result = await database.manyOrNone<Row>(
    'SELECT'
      + ' article.identifier, article.name, article.description, article.date_published'
      + ', author.author'
      + ' FROM scholarly_articles article'
      + ' JOIN scholarly_article_authors author ON author.scholarly_article=article.identifier'
      + ' WHERE article.date_published IS NOT NULL'
      + ' ORDER BY article.date_published DESC'
      // tslint:disable-next-line:no-invalid-template-strings
      + ' LIMIT ${limit}',
    {
      limit,
    },
  );

  const articles: PublishedScholarlyArticle[] = result.map((row) => {
    return {
      author: [ { identifier: row.author } ],
      datePublished: row.date_published.toISOString(),
      description: row.description,
      identifier: row.identifier,
      name: row.name,
    };
  });

  return articles;
}

export async function fetchScholarlyArticlesByAuthor(
  database: Database,
  authorId: string,
): Promise<Array<Partial<ScholarlyArticle>>> {
  interface Row {
    identifier: string;
    name: string;
    description: string;
    author: string;
    date_published?: Date;
    same_as?: string;
  }
  const result = await database.manyOrNone<Row>(
    'SELECT'
      + ' article.identifier, article.name, article.description'
      + ', author.author'
      + ', same_as.same_as'
      + ' FROM scholarly_articles article'
      + ' JOIN scholarly_article_authors author ON author.scholarly_article=article.identifier'
      + ' LEFT JOIN scholarly_articles_same_as same_as ON same_as.scholarly_article=article.identifier'
      + ' LEFT JOIN scholarly_articles_part_of submission ON submission.scholarly_article=article.identifier'
      // tslint:disable-next-line:no-invalid-template-strings
      + ' WHERE author.author=${authorId}'
      // Make sure the article isn't being submitted somewhere but not published yet
      + ' AND ('
        + 'article.date_published IS NOT NULL'
        + ' OR (submission.is_part_of IS NOT NULL AND submission.date_published IS NOT NULL)'
      + ')'
      + ' ORDER BY LEAST(article.date_published, submission.date_published) DESC',
    {
      authorId,
    },
  );

  const articles: Array<Partial<ScholarlyArticle>> = result.map((row) => {
    return {
      datePublished: (row.date_published) ? row.date_published.toISOString() : undefined,
      description: row.description,
      identifier: row.identifier,
      name: row.name,
      sameAs: row.same_as,
    };
  });

  return articles;
}
